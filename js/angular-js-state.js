bCao.controller('angular-js-state', function ($scope, $location) {
    $scope.tenModel = '{{ }}';
    $scope.ngRouteScript = '<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular-route.js"></script>';
    $scope.angularScript = '<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>';
    $scope.angState1 = true;
    $scope.angState2 = true;
    $scope.angState3 = true;
    $scope.angState4 = true;
    $scope.angState5 = true;
    $scope.angState6 = true;
    $scope.angState7 = true;
    $scope.angState8 = true;
    $scope.angState9 = true;
    $scope.angState10 = true;

    $scope.angFState1 = function () {
        $location.path('/angular-js');
        $scope.angState1 = true;
        $scope.angState2 = true;
        $scope.angState3 = true;
        $scope.angState4 = true;
        $scope.angState5 = true;
        $scope.angState6 = true;
        $scope.angState7 = true;
        $scope.angState8 = true;
        $scope.angState9 = true;
        $scope.angState10 = true;
    };

    $scope.angFState2 = function () {
        $location.path('/angular-js');
        $scope.angState1 = false;
        $scope.angState2 = false;
        $scope.angState3 = true;
        $scope.angState4 = true;
        $scope.angState5 = true;
        $scope.angState6 = true;
        $scope.angState7 = true;
        $scope.angState8 = true;
        $scope.angState9 = true;
        $scope.angState10 = true;
    };

    $scope.angFState3 = function () {
        $location.path('/angular-js');
        $scope.angState1 = false;
        $scope.angState2 = true;
        $scope.angState3 = false;
        $scope.angState4 = true;
        $scope.angState5 = true;
        $scope.angState6 = true;
        $scope.angState7 = true;
        $scope.angState8 = true;
        $scope.angState9 = true;
        $scope.angState10 = true;
    };

    $scope.angFState4 = function () {
        $location.path('/angular-js');
        $scope.angState1 = false;
        $scope.angState2 = true;
        $scope.angState3 = true;
        $scope.angState4 = false;
        $scope.angState5 = true;
        $scope.angState6 = true;
        $scope.angState7 = true;
        $scope.angState8 = true;
        $scope.angState9 = true;
        $scope.angState10 = true;
    };

    $scope.angFState5 = function () {
        $location.path('/angular-js');
        $scope.angState1 = false;
        $scope.angState2 = true;
        $scope.angState3 = true;
        $scope.angState4 = true;
        $scope.angState5 = false;
        $scope.angState6 = true;
        $scope.angState7 = true;
        $scope.angState8 = true;
        $scope.angState9 = true;
        $scope.angState10 = true;
    };

    $scope.angFState6 = function () {
        $location.path('/angular-js');
        $scope.angState1 = false;
        $scope.angState2 = true;
        $scope.angState3 = true;
        $scope.angState4 = true;
        $scope.angState5 = true;
        $scope.angState6 = false;
        $scope.angState7 = true;
        $scope.angState8 = true;
        $scope.angState9 = true;
        $scope.angState10 = true;
    };

    $scope.angFState7 = function () {
        $location.path('/angular-js');
        $scope.angState1 = false;
        $scope.angState2 = true;
        $scope.angState3 = true;
        $scope.angState4 = true;
        $scope.angState5 = true;
        $scope.angState6 = true;
        $scope.angState7 = false;
        $scope.angState8 = true;
        $scope.angState9 = true;
        $scope.angState10 = true;
    };

    $scope.angFState8 = function () {
        $location.path('/angular-js');
        $scope.angState1 = false;
        $scope.angState2 = true;
        $scope.angState3 = true;
        $scope.angState4 = true;
        $scope.angState5 = true;
        $scope.angState6 = true;
        $scope.angState7 = true;
        $scope.angState8 = false;
        $scope.angState9 = true;
        $scope.angState10 = true;
    };

    $scope.angFState9 = function () {
        $location.path('/angular-js');
        $scope.angState1 = false;
        $scope.angState2 = true;
        $scope.angState3 = true;
        $scope.angState4 = true;
        $scope.angState5 = true;
        $scope.angState6 = true;
        $scope.angState7 = true;
        $scope.angState8 = true;
        $scope.angState9 = false;
        $scope.angState10 = true;
    };

    $scope.angFState10 = function () {
        $location.path('/angular-js');
        $scope.angState1 = false;
        $scope.angState2 = true;
        $scope.angState3 = true;
        $scope.angState4 = true;
        $scope.angState5 = true;
        $scope.angState6 = true;
        $scope.angState7 = true;
        $scope.angState8 = true;
        $scope.angState9 = true;
        $scope.angState10 = false;
    };
});