var bCao = angular.module('baoCaoCaNhan', ['ngRoute']);
bCao.config(function ($routeProvider) {
    $routeProvider
        .when('/', { templateUrl: '/document/trang-chu.htm' })
        .when('/angular-js', { templateUrl: '/document/angular-js.htm', controller: 'angular-js-state' })
        .when('/node-js', { templateUrl: '/document/node-js.htm', controller: 'node-js-state'})
        .when('/c-sharp', { templateUrl: '/document/c-sharp.htm', controller: 'c-sharp-state'})
});